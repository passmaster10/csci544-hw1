import pickle
import sys
import math
modelfile=sys.argv[1]
testfile=sys.argv[2]
masterdict=pickle.load(open(modelfile,'rb'))
f=open(testfile,'r')
lines=f.readlines()
lines=[line.replace('\n',' ') for line in lines]
classes=[]
probc={}
n={}
for key in masterdict:
 if(key!='freqtot'and key!='totvocab' and key!='totfilecount'):
  classes.append(key)
  probc[key]=masterdict[key]['filecount']/masterdict['totfilecount']
#print(classes)
for c in classes:
 n[c]=sum(masterdict[c]['freqc'].values())
#fout=open('spam.out','w')
for line in lines:
 temp=line.split(' ')
 score={}
 pwordgc={}
 for c in classes:
  pwordgc[c]={}
 for word in temp:
  for c in classes:
   pwordgc[c][word]=masterdict[c]['probc'][word] if word in masterdict[c]['probc'] else math.log(1/(n[c]+masterdict['totvocab']))
 for c in classes:
  score[c]=sum(pwordgc[c].values())+math.log(probc[c])
 print(max(score,key=score.get))#get key in score with highest value
#fout.close()
