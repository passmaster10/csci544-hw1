NAIVE BAYES CLASSIFIER:
SPAM: Precision=94.01% Recall=99.44% F-Score=96.65%
HAM: Precision=99.79% Recall=97.70% F-Score=98.73%
POS: Precision=88.88% Recall=78.29% F-Score=83.25%
NEG: Precision=80.97% Recall=90.41% F-Score=85.43%

SVM:
SPAM: Precision=98.03% Recall=95.87% F-Score=96.93%
HAM: Precision=98.51% Recall=99.30% F-Score=98.90%
POS: Precision=86.93% Recall=87.02% F-Score=86.97%
NEG: Precision=87.29% Recall=87.29% F-Score=87.29%

MEGAM:
SPAM: Precision=97.23% Recall=96.96% F-Score=97.10%
HAM: Precision=98.90% Recall=99.00% F-Score=98.95%
POS: Precision=85.93% Recall=87.12% F-Score=86.04%
NEG: Precision=87.22% Recall=86.04% F-Score=86.62%

If we use only 10% of training data to train classifiers in part 1 and part 2, the precision, recall and f-score will reduce.
Particularly in this classification problem, we either ignore words in the development/test data that we did not encounter in the training data (this is the case for part 2) or we assign a very low probability to them (this is the case in part 1). 
This means that the number of words that contribute towards the conditional probabilities of each class in a document will be low.
This further means higher false positives (lower precision), higher false negatives(lower recall).
