import sys
import os
trainingdir=sys.argv[1]
trainingfile=sys.argv[2]
testdir=sys.argv[3]
testfile=sys.argv[4]
filelist=os.listdir(trainingdir)
fout=open(trainingfile,'w')
enumword={}
count=1
for trfile in filelist:
 
 with open(trainingdir+'/'+trfile,errors="ignore")as f:
  fcontents=f.read().replace('\n',' ')
 temp=fcontents.split(' ') 
 for word in temp:
  if word not in enumword: 
   enumword[word]=count
   count+=1

for trfile in filelist:
 classname=trfile.split('.')[0]
 freqword={}
 with open(trainingdir+'/'+trfile,errors="ignore") as f:
  fcontents=f.read().replace('\n',' ')
 temp=fcontents.split(' ') 
 for word in temp:
  if word not in freqword:
   freqword[enumword[word]]=1
  else:
   freqword[enumword[word]]+=1
 if(classname=='SPAM' or classname=='POS'):
  fout.write('+1')
 else:
  fout.write('-1')
 for key in sorted(freqword):
  fout.write(' '+str(key)+':'+str(freqword[key]))
 fout.write('\n')
fout.close()


fout=open(testfile,'w')
filelist=os.listdir(testdir)
filelist.sort()
for testfile in filelist:
 #classname=testfile.split('.')[0] 
 freqword={}
 with open(testdir+'/'+testfile,errors="ignore") as f:
  fcontents=f.read().replace('\n',' ')
 temp=fcontents.split(' ')
 for word in temp:
  if word in enumword:
   if word not in freqword:
    freqword[enumword[word]]=1
   else:
    freqword[enumword[word]]+=1
 '''if(classname=='SPAM' or classname=='POS'): 
  fout.write('+1')
 else:
  fout.write('-1')'''
 fout.write('0')
 for key in sorted(freqword):
  fout.write(' '+str(key)+':'+str(freqword[key]))
 fout.write('\n')
fout.close()

 
 
