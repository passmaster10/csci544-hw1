import pickle
import sys
import math
trainingfile=sys.argv[1]
modelfile=sys.argv[2]
f=open(trainingfile,'r')
masterdict={'freqtot':{},'totvocab':0,'totfilecount':0}
lines=[]
count=0
#for line in f:
lines=f.readlines()
lines=[line.replace('\n',' ') for line in lines]
#lines=[line.rstrip() for line in lines]
masterdict['totfilecount']=len(lines)
#print(masterdict['totfilecount'])
for line in lines:
 temp=line.split(' ')
 c=temp[0]
 temp.pop(0)
 if c not in masterdict:
  #print(c)
  masterdict[c]={'freqc':{},'filecount':1,'probc':{}}
  #print(masterdict[c])
  for word in temp:
   if word not in masterdict['freqtot']:masterdict['freqtot'][word]=1
   else: masterdict['freqtot'][word]+=1
   if word not in masterdict[c]['freqc']:
    masterdict[c]['freqc'][word]=1
   else:
    masterdict[c]['freqc'][word]+=1
 else:
  #if c=='SPAM': count+=1
  masterdict[c]['filecount']+=1
  for word in temp:
   if word not in masterdict['freqtot']:masterdict['freqtot'][word]=1
   else: masterdict['freqtot'][word]+=1
  
   if word not in masterdict[c]['freqc']:
    masterdict[c]['freqc'][word]=1
   else:
    masterdict[c]['freqc'][word]+=1
   
#print(count)
masterdict['totvocab']=len(masterdict['freqtot'])
#print(str(masterdict)+'\n\n\n\n\n'+'spam'+'\n')
#print(masterdict['HAM']['freqc'])
#print(masterdict['SPAM']['freqc'])
for c in masterdict:
 
 if(c!='freqtot'and c!='totvocab' and c!='totfilecount'):
  n=sum(masterdict[c]['freqc'].values())
  for word in masterdict[c]['freqc']:
   masterdict[c]['probc'][word]=math.log((masterdict[c]['freqc'][word]+1)/(n+masterdict['totvocab']))
   #masterdict[c]['probc'][word]=math.log(masterdict[c]['probc'][word])  
pickle.dump(masterdict,open(modelfile,"wb")) 
#print(masterdict['totfilecount'])
#print(masterdict['totvocab'])
#print(masterdict['freqtot'])

#print(masterdict)
  
'''freqspam={}
freqham={}
freqtot={}
lines=[]
spamcount=0
hamcount=0
totcount=0
for line in f:
 lines.append(line)
lines=[line.replace('\n',' ') for line in lines]
lines=[line.rstrip() for line in lines]
#print(lines)
for line in lines:
 temp=line.split(' ')
 if(temp[0]=='SPAM'):
  temp.pop(0)
  spamcount+=1
  totcount+=1
  for word in temp:
   if word:
    if word not in freqspam:
     freqspam[word]=1
    else:
     freqspam[word]+=1
    if word not in freqtot:
     freqtot[word]=1
    else:
     freqtot[word]+=1
 elif(temp[0]=='HAM'):
  temp.pop(0)
  hamcount+=1
  totcount+=1	
  for word in temp:
   if word:
    if word not in freqham:
     freqham[word]=1
    else:
     freqham[word]+=1
    if word not in freqtot:
     freqtot[word]=1
    else:
     freqtot[word]+=1
#print(freqspam)
#print(freqham)
#print(freqtot)
totvocab=len(freqtot)
#print("vocabulary="+str(totvocab))
probspam={}
probham={}
totspam=sum(freqspam.values())
totham=sum(freqham.values())
#print(totspam)
#print(totham)
for word in freqspam:
 probspam[word]=(freqspam[word]+1)/(totspam+totvocab)
for word in freqham:
 probham[word]=(freqham[word]+1)/(totham+totvocab)
#print(probspam)
#print(probham)
for key in probspam:probspam[key]=math.log(probspam[key])
for key in probham:probham[key]=math.log(probham[key])
#print(probham)
masterdict={}
masterdict['freqspam']=freqspam
masterdict['freqham']=freqham
masterdict['totvocab']=totvocab
masterdict['probspam']=probspam
masterdict['probham']=probham
masterdict['spamcount']=spamcount
masterdict['hamcount']=hamcount
masterdict['totcount']=totcount
pickle.dump(masterdict,open(modelfile,"wb")) 
print(masterdict)'''
